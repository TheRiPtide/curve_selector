import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


def animate(x, y, filename):

    fig, ax = plt.subplots()
    line, = ax.plot(x, y, color='k')

    def update(num, x, y, line):
        line.set_data(x[:num], y[:num])
        return line,

    ani = animation.FuncAnimation(fig, update, len(x), fargs=[x, y, line],
                                  interval=25, blit=True)
    ani.save(f'{filename}.mp4')
    plt.close(fig)


def naive_sampler(num_samples, data):
    """
    A sampler that gives a specific number of samples evenly spaced through the dataset
    :param num_samples: Number of Samples
    :type num_samples: int
    :param data: List of Curve Data
    :type data: List[float]
    :return: Samples index
    :rtype: List[float]
    """
    samples = [0.0] * num_samples

    sample_distance = len(data) / num_samples

    for i in range(num_samples):
        samples[i] = data[int(np.round(i * sample_distance))]

    samples[-1] = data[-1]

    return samples