# curve_selector

requirements:
   - python 3.8
   - pandas
   - numpy
   - matplotlib
   
run the file with 

    python plot_and_select.py
    
to execute the script. By default it will allow you select a directory where the raw files are located.

Then it will plot each file for you and ask if you want to keep it or not.
Confirmed files will be copied to a  `selected_files` subfolder.

Run it with flag `--log` to have a logarithmic scale on the y-axis
and/or run it with `--all` to simply plot all files in the directory and save them at the same place as the raw files.

With the flag `--all` you can use `--sep` to plot each single curve of one sweep in a seperate file and/or `--mov` to 
animate the sweep (as a whole or as seperate curves). It is saved as mp4 movie. 

Optional, add --folder "D:/my_pictures/selected_files" to add the folder with the *.dat files directly, without file selection promt. (Optimal to run tests and scripts with it.)