import argparse
import tkinter as tk
from tkinter import filedialog, messagebox
import glob
import os
import pandas as pd
from matplotlib import pyplot as plt
from pathlib import Path
import shutil
import pathlib
import numpy as np
import linecache
import tools

from matplotlib.offsetbox import AnchoredText
from matplotlib.patches import ConnectionPatch

parser = argparse.ArgumentParser(
    description='Plot all files in directory and select which to save to a different directory')
parser.add_argument('-a', '--all', help='Plot all files in directory', action='store_true')
parser.add_argument('-l', '--log', help='Log scale on y axis', action='store_true')
parser.add_argument('-m', '--mov', help='Make animation', action='store_true')
parser.add_argument('-s', '--sep', help='Single Lines in plot', action='store_true')
parser.add_argument('-f', '--folder', help='Add directly the folder with our file as command-line argument',
                    action='store')
args = parser.parse_args()

if (not args.all and args.sep) or (not args.all and args.mov):
    parser.error('The --sep and or --mov argument is only possible with the --all argument')

root = tk.Tk()
root.withdraw()

if (not args.folder):
    dir_string = filedialog.askdirectory()
else:
    dir_string = args.folder
dir = Path(dir_string)

new_dir = dir / 'selected_files'

if not args.all:
    try:
        os.mkdir(new_dir)
    except:
        pass

scheme = dir / '*.dat'

# scheme = scheme.replace('/', '\\')
file_path = str(scheme.absolute())

files = glob.glob(file_path)

if (len(files) > 0):
    print("Thanks! I found " + str(len(files)) + " files and started working now. Hang on pls!")
else:
    print("No files found. Are you sure about the path?" + file_path)

files_to_keep = []


def add_text_to_plot(max_v, max_i, series_resist, sweep_freq, num_curves, this_curve=0):

    v_input = np.round(max_v + max_i * series_resist, 3)
    if (this_curve == 0):
        text_box = '\n'.join((f'sample: {sample_name}', f'est. input V: {v_input}', f'series R: {series_resist}',
                              f'sweep frequency: {sweep_freq}', f'# curves: {len(num_curves)}'))
    else:
        text_box = '\n'.join((f'sample: {sample_name}', f'est. input V: {v_input}', f'series R: {series_resist}',
                              f'sweep frequency: {sweep_freq}', f'# curves: {len(num_curves)}',
                              f'# actual curve: {this_curve}'))

    anchored_text = AnchoredText(text_box, loc=4)
    ax.add_artist(anchored_text)
    plt.text(0.05, 0.95, text_box)


for file in files:

    df = pd.read_csv(file, delimiter='	', skiprows=1)

    first_line = linecache.getline(file, 1).replace('\n', '')
    linecache.clearcache()

    line_list = first_line.split(' ')

    gain = int(float(line_list[1]))
    sampling_rate = int(float(line_list[6]))
    series_resist = int(float(line_list[11]))

    num_curves = df['Curve #'].unique()

    filename = pathlib.Path(file).stem

    sample_name_list = filename.split('_')[:-2]
    sample_name = '-'.join(sample_name_list)

    f, ax = plt.subplots(1, 1)
    legends = []
    if args.log:
        plt.yscale('log')
        df['Current (A)'] = df['Current (A)'].abs()

        filename = filename + '_log'

    max_v_idx = df.iloc[:, 1].idxmax()
    max_i = df.iloc[max_v_idx, 2]
    max_v = df.iloc[max_v_idx, 1]

    for num in num_curves:
        subdf = df[df['Curve #'] == num]

        if num == 1:
            # plt.cla()
            num_points = len(subdf.index)
            sweep_freq = np.round(sampling_rate / num_points)

        ax.plot(subdf.iloc[:, 1], subdf.iloc[:, 2], linewidth=0.3, alpha=0.3)
        if args.all:

            if args.sep:
                add_text_to_plot(max_v, max_i, series_resist, sweep_freq, num_curves, num)
                plt.savefig(dir / (filename + '_' + str(num) + '.png'))
                plt.cla()
                if args.mov:
                    x = tools.naive_sampler(200, subdf.iloc[:, 1].tolist())
                    y = tools.naive_sampler(200, subdf.iloc[:, 2].tolist())
                    print("Creating movies takes some time..." + filename + '_' + str(num))
                    tools.animate(x, y, dir / (filename + '_' + str(num)))

        if (not args.sep and len(num_curves) < 10):
            legends.append(num)

    if len(legends) > 0:
        lg = plt.legend(legends)
        for lh in lg.legendHandles:
            lh.set_alpha(1)

    add_text_to_plot(max_v, max_i, series_resist, sweep_freq, num_curves, 0)

    if args.all:

        if not args.sep:

            plot_name = dir / (filename + '.png')
            plt.savefig(plot_name)

            if args.mov:
                x = tools.naive_sampler(1000, df.iloc[:, 1].tolist())
                y = tools.naive_sampler(1000, df.iloc[:, 2].tolist())

                print("Creating movies takes some time.. " + filename + ".mp4")
                tools.animate(x, y, dir / filename)

    else:

        plt.show(block=False)

        reply = messagebox.askyesnocancel(message='Keep file?')

        if reply:

            plot_name = new_dir / (filename + '.png')
            files_to_keep.append(file)
            plt.savefig(plot_name)
            shutil.copy(file, new_dir)

        elif reply is None:

            break

    plt.close(f)

print("Ok, finished! Please have a look here:  " + str(dir.absolute()))
